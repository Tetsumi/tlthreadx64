#!/bin/bash

function nasm_compile
{
    nasm -I ./source/ -felf64 ./source/$1.asm -o ./temp/$1.o
}

nasm_compile thread

nasm -I ./source/ -felf64 ./test/main.asm -o ./temp/main.o
gcc -g ./temp/thread.o ./temp/main.o -o test_asm.out
gcc -g ./temp/thread.o ./test/test.c -o test_c.out
