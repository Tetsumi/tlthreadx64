/*
;;;
;;;   tlthreadx64
;;;
;;;   Copyright 2015 Tetsumi <tetsumi@vmail.me>
;;; 
;;;   This program is free software: you can redistribute it and/or modify
;;;   it under the terms of the GNU General Public License as published by
;;;   the Free Software Foundation, either version 3 of the License, or
;;;   (at your option) any later version.
;;;
;;;   This program is distributed in the hope that it will be useful,
;;;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;   GNU General Public License for more details.
;;;
;;;   You should have received a copy of the GNU General Public License
;;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;; 
*/

#include "../source/tlthreadx64.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdatomic.h>
#include <assert.h>
#include <stdarg.h>

atomic_uint g_barrier = 0;
atomic_flag g_printflock =  ATOMIC_FLAG_INIT;

int sync_printf(const char const *format, ...)
{
	va_list args;
	va_start(args, format);

	while (atomic_flag_test_and_set_explicit(&g_printflock,
						 memory_order_acquire))
	{
		THREAD_sleep(0, 10000);
	}
	
	vprintf(format, args);
	atomic_flag_clear_explicit(&g_printflock, memory_order_release);
	va_end(args);
}

int foo (uint64_t argument)
{
	THREAD_Id id = 0;
	THREAD_identifier(&id);
	
	sync_printf("THREAD [ID %u]: Started with argument %u\n", id, argument);

	--g_barrier;

	sync_printf("THREAD [ID %u]: End\n", id);
	
	return EXIT_SUCCESS;
}

int main (void)
{
	puts("THREAD [MAIN]: start");
	
	THREAD_Id thread = 0;

	for (uint64_t i = 0; i < 10; ++i)
	{
		++g_barrier;
		THREAD_Status status = THREAD_create(foo, (void*)i, &thread);
		assert(THREAD_SUCCESS == status);
		sync_printf("THREAD [MAIN]: Thread created (%u)\n", thread);
	}

	while (g_barrier)
		THREAD_sleep(1, 0);

	puts("THREAD [MAIN]: end");
	
	return EXIT_SUCCESS;
}
