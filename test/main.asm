;;;
;;;   tlthreadx64
;;;
;;;   Copyright 2015 Tetsumi <tetsumi@vmail.me>
;;; 
;;;   This program is free software: you can redistribute it and/or modify
;;;   it under the terms of the GNU General Public License as published by
;;;   the Free Software Foundation, either version 3 of the License, or
;;;   (at your option) any later version.
;;;
;;;   This program is distributed in the hope that it will be useful,
;;;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;   GNU General Public License for more details.
;;;
;;;   You should have received a copy of the GNU General Public License
;;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;; 

%include "linux.inc"

global main

extern THREAD_create
extern THREAD_sleep
extern THREAD_identifier	
extern printf

section .rodata

	strThread db `Hello from thread %u\n\0`
	strDbgTh  db `- THREAD: id: %u\n\0`
	strDbg    db `-   MAIN: id: %u\n\0`
	
section .data
	
timeval:
	tv_sec    dq 3
	tv_usec   dq 0
	
section .text

thread_func:
	push rax
	xor rax, rax
	mov rsi, rdi
	mov rdi, strThread
	call printf

	mov rdi, rsp
	call THREAD_identifier
	xor rax, rax
	mov rsi, [rsp]
	mov rdi, strDbgTh
	call printf
	pop rdx
	ret
	
main:
	push rax
	mov rdi, thread_func
	mov rsi, 123
	mov rdx, rsp
	call THREAD_create

	mov rdi, strDbg
	mov rsi, [rsp]
	call printf
	
	; sleep 5 seconds
	mov rdi, 5
	mov rsi, 0
	call THREAD_sleep
	
	xor rax, rax
	pop rdx
	ret
