#include <stdint.h>

#define THREAD_VERSION_MAJOR 1
#define THREAD_VERSION_MINOR 0
#define THREAD_VERSION_REVISION 0
#define THREAD_VERSION_STRING "1.0.0"

typedef enum
{
	THREAD_SUCCESS,
	THREAD_ERROR,
	THREAD_NOMEM
} THREAD_Status;


typedef uint64_t THREAD_Id;

THREAD_Status THREAD_create (int (*const f)(), void *argument, THREAD_Id *tid);
void          THREAD_exit   (int exitcode);
THREAD_Status THREAD_yield  (void);
THREAD_Status THREAD_sleep  (uint64_t seconds, uint64_t nanoseconds);
THREAD_Status THREAD_identifier (THREAD_Id *id);
