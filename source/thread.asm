;;;
;;;   tlthreadx64
;;;
;;;   Copyright 2015 Tetsumi <tetsumi@vmail.me>
;;; 
;;;   This program is free software: you can redistribute it and/or modify
;;;   it under the terms of the GNU General Public License as published by
;;;   the Free Software Foundation, either version 3 of the License, or
;;;   (at your option) any later version.
;;;
;;;   This program is distributed in the hope that it will be useful,
;;;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;   GNU General Public License for more details.
;;;
;;;   You should have received a copy of the GNU General Public License
;;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;; 

%include "linux.inc"
%include "thread.inc"
	
	; Imports
extern printf
	
	; Exports
global THREAD_create
global THREAD_exit
global THREAD_yield
global THREAD_sleep
global THREAD_identifier
	
section .rodata
	
	stri db `%u %u %u %u\n\0`
	
section .text

;;; [PUBLIC] THREAD_create
;;;
;;; Creates a new thread.
;;;
;;; ARGUMENTS:
;;;   RDI -> Address of function to execute.
;;;   RSI -> Argument to pass to the function.
;;;   RDX -> Address of memory location to put the identifier of the new thread.
;;;          Can be null.
;;; 
;;; RETURN:
;;;   THREAD_SUCCESS If the creation of the new thread was successful.
;;;   THREAD_NOMEM   If there was insufficient amount of memory.
;;;   THREAD_ERROR   If another error occurred.
THREAD_create:
	; Verify arguments
	test rdi, rdi
	jz .error

	; Save arguments
	push rdx
	push rsi
	push rdi

	; Allocate stack + TLS
	mov rax, SYS_MMAP
	xor rdi, rdi
	lea rsi, [THREAD_STACKSIZE]
	mov rdx, PROT_READWRITE
	mov r10, MAP_PRIVANON
	xor r8, r8
	xor r9, r9
	syscall
	cmp rax, -4096
	ja .error_memory
	
	; Align on 16 bytes
	mov r8, rax
	add r8, 16
	and r8, 0xFFFFFFFFFFFFFFF0

	; Set the stack's address to the top minus the return address
	lea rsi, [r8 + THREAD_STACKSIZE - 8]

	; Set the return address to wrapper's address
	mov qword [rsi], wrapper_function

	; Save wrapper's arguments on thread's call stack
	mov [r8 + THREAD_STACKSIZE - 16], rax
	pop qword [r8 + THREAD_STACKSIZE - 24]
	pop qword [r8 + THREAD_STACKSIZE - 32]

	; Set thread's identifier (address of TLS)
	pop rdx
	cmp rdx, 0
	je .pass_tls
	mov [rdx], rax	
.pass_tls:
	; Create the thread
	mov rax, SYS_CLONE
	mov rdi, THREAD_FLAGS
	xor rdx, rdx
	xor r10, r10
	syscall
	cmp rax, -4096
	ja .error_clone
	xor rax, rax
	ret
.error_clone:
	; Free thread's stack memory
	mov rax, SYS_MUNMAP
	mov rdi, rax
	mov rsi, THREAD_STACKSIZE
	syscall
.error:
	mov rax, THREAD_ERROR
	ret
.error_memory:
	; Ditch saved arguments
	add rsp, 24
	mov rax, THREAD_NOMEM
	ret

;;; [PRIVATE] wrapper_function
;;;
;;; Setups the thread then call the thread's procedure.
;;;
;;; ARGUMENTS:
;;;   None.
;;;
;;; RETURN:
;;;   None.
wrapper_function:
	; Set TLS in GS
	mov rax, SYS_ARCH_PRCTL
	mov rdi, ARCH_SET_GS
	mov rsi, [rsp - 16]
	syscall

	; Set thread's identifier (Address of TLS)
	mov [gs:0], rsi
		
	; Set procedure's argument
	mov rdi, [rsp - 32]
	
	; Call the procedure
	call [rsp - 24]
	
	; Exit (tail call)
	mov edi, eax
	jmp THREAD_exit

;;; [PUBLIC] THREAD_exit
;;;
;;; Exits the calling thread.
;;;
;;; ARGUMENTS:
;;;   RDI -> exit code.
;;;
;;; RETURN:
;;;   None.
THREAD_exit:
	; Get TLS from GS
	mov r10, rdi
	mov rax, SYS_ARCH_PRCTL
	mov rdi, ARCH_GET_GS
	lea rsi, [rsp - 8]
	syscall
	
	; Free call stack's memory
	mov rax, SYS_MUNMAP
	mov rdi, [rsp - 8]
	mov rsi, THREAD_STACKSIZE
	syscall

	; Exit with 1 if munmap failed
	mov rdi, r10
	mov rdx, 1
	cmp rax, -4096
	cmova rdi, rdx

	; Exit
	mov rax, SYS_EXIT
	syscall
	hlt

;;; [PUBLIC] THREAD_yield
;;;
;;; Yields the calling thread.
;;;
;;; ARGUMENTS:
;;;   None.
;;;
;;; RETURN:
;;;   THREAD_SUCCESS
;;;   THREAD_ERROR
THREAD_yield:
	mov rax, SYS_SCHED_YIELD
	syscall
	mov rcx, THREAD_ERROR
	mov rdx, THREAD_SUCCESS
	cmp rax, -4096
	mov rax, rdx
	cmova rax, rcx
	ret

;;; [PUBLIC] THREAD_sleep
;;;
;;; Suspends execution of the calling thread for the given period of time. 
;;;
;;; ARGUMENTS:
;;;   RDI -> Seconds.
;;;   RSI -> Nanoseconds.
;;;
;;; RETURN:
;;;   THREAD_SUCCESS If successful.
;;;   THREAD_ERROR   If an error occurred.
THREAD_sleep:
	push rsi
	push rdi
	mov rax, SYS_NANOSLEEP
	mov rdi, rsp
	xor rsi, rsi
	syscall
	mov rcx, THREAD_ERROR
	mov rdx, THREAD_SUCCESS
	cmp rax, -4096
	mov rax, rdx
	cmova rax, rcx
	add rsp, 16
	ret

;;; [PUBLIC] THREAD_identifier
;;;
;;; Returns identifier of calling thread. 
;;;
;;; ARGUMENTS:
;;;   RDI -> Address of memory location to put the identifier.
;;;
;;; RETURN:
;;;   THREAD_SUCCESS If successful.
;;;   THREAD_ERROR   If an error occurred.
THREAD_identifier:
	test rdi, rdi
	jz .error
	mov rdx, [gs:0]
	mov [rdi], rdx
	xor rax, rax
	ret
.error:
	mov rax, THREAD_ERROR
	ret
